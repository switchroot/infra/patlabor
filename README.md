# Patlabor

A fork of [Robocop-NG](https://github.com/reswitched/robocop-ng) for use on the Switchroot discord server. 

Code is based on https://gitlab.com/ao/dpybotbase and https://github.com/916253/Kurisu-Reswitched.

---

## How to run

- Copy `config.py.template` to `config.py`, configure all necessary parts to your server.
- Install python3.6+.
- Install python dependencies (`pip3 install -Ur requirements.txt`, you might need to put `sudo -H` before that)
- Run `Patlabor.py` (`python3 Patlabor.py`)

To keep the bot running, you might want to use pm2 or a systemd service.

---

## Credits
Patlabor is based on [Robocop-NG.](https://github.com/reswitched/robocop-ng) Changes were made to the bot as necessary for use on Switchroot.

Huge thanks to the developers and maintainers of Robocop-NG and Kurisu for their hard work.

From the original Robocop-NG repo:

>Robocop-NG is currently developed and maintained by @aveao and @tumGER. The official bot is hosted by @yuukieve.
>
>I (ave) would like to thank the following, in no particular order:
>
>- ReSwitched community, for being amazing
>- ihaveamac/ihaveahax and f916253 for the original kurisu/robocop
>- misson20000 for adding in reaction removal feature and putting up with my many BS requests on PR reviews
>- Everyone who contributed to robocop-ng in any way (reporting a bug, sending a PR, forking and hosting their own at their own guild, etc).

