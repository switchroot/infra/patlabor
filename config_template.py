import datetime

# Basic bot config, insert your token here, update description if you want
prefixes = [".", "!"]
token = "token-goes-here"
bot_description = "Patlabor, the moderation bot of switchroot."

# If you forked robocop-ng, put your repo here
source_url = "https://gitlab.com/switchroot/patlabor"
rules_url = "https://pastebin.com/raw/akuMKXPC"

# The bot description to be used in .robocop embed
embed_desc = "Patlabor is developed by sirocyl and Ave, "\
             "and is based on Robocop-NG by [Ave](https://github.com/aveao)"\
             " and [tomGER](https://github.com/tumGER), which is a rewrite "\
             "of Robocop.\nRobocop is based on Kurisu by 916253 and ihaveamac."


# Minimum account age required to join the guild
# If user's account creation is shorter than the time delta given here
# then user will be kicked and informed
min_age = datetime.timedelta(minutes=15)

# The bot will only work in these guilds
guild_whitelist = [
    521977609182117891  # switchroot
]

# Named roles to be used with .approve and .revoke
# Example: .approve User hacker
named_roles = {
    "developer": 545096637572317207,
    "contributor": 593798924955942932,
    "user": 545097019920875521,
    "tester": 588128252292497420
}

# The bot manager and staff roles
# Bot manager can run eval, exit and other destructive commands
# Staff can run administrative commands
bot_manager_role_id = 604424642148433933  # Bot management role
staff_role_ids = [604424642148433933,  # Bot management
                  521995479463100427,  # management
                  521995056937304065,  # Administration
                  521995431715274752,  # operations
                  604672240818192404]  # mods

# Various log channels used to log bot and guild's activity
# You can use same channel for multiple log types
# Spylog channel logs suspicious messages or messages by members under watch
# Invites created with .invite will direct to the welcome channel.
log_channel = 521998175146475521  # server-logs
botlog_channel = 588506597513494528  # bot-logs
modlog_channel = 588506470694256640  # mod-logs
spylog_channel = 588506632166703104  # watch-logs
welcome_channel = 588507039710314502  # entryway
participant_role = 545097019920875521  # user role

# These channel entries are used to determine which roles will be given
# access when we unmute on them
general_channels = [604648722491768883,
                    604649938575687690,
                    545098728013561866,
                    588138053735022632]
community_channels = []

# Controls which roles are blocked during lockdown
lockdown_configs = {
    # Used as a default value for channels without a config
    "default": {
        "channels": general_channels,
        "roles": [named_roles["user"]]
    }
}

# Mute role is applied to users when they're muted
# As we no longer have mute role on ReSwitched, I set it to 0 here
mute_role = 0  # Mute role in ReSwitched

# Channels that will be cleaned every minute/hour
minutely_clean_channels = []
hourly_clean_channels = []

# Edited and deletes messages in these channels will be logged
spy_channels = general_channels

# Channels and roles where users can pin messages
allowed_pin_channels = []
allowed_pin_roles = []

# Used for the pinboard. Leave empty if you don't wish for a gist pinboard.
github_oauth_token = ""
